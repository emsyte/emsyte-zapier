/* globals describe, it */

require("should");

const zapier = require("zapier-platform-core");

zapier.tools.env.inject();

const App = require("../index");
const appTester = zapier.createAppTester(App);

const registration = {
  hook: {
    id: 4,
    event: "registration",
    target: "http://localhost:5000"
  },
  data: {
    event: {
      start_time: "2020-04-07T08:30:00-05:00",
      origin_timezone: "US/Central",
      origin_start_time: "2020-04-07T08:30:00-05:00"
    },
    webinar: {
      id: 9,
      name: "Test",
      video: 5,
      published: true,
      start_date: "2020-03-01",
      stop_date: null
    },
    name: "joe",
    email: "joe@toptal.com",
    created: "2020-03-26T23:13:42.173228-05:00"
  }
};

const registrantMissedWebinarEvent = {
  hook: {
    id: 4,
    event: "registration",
    target: "http://localhost:5000"
  },
  data: {
    id: 17,
    created: "2020-03-27T13:42:03.710664-05:00",
    event: {
      start_time: "2020-04-07T08:30:00-05:00",
      origin_timezone: "US/Central",
      origin_start_time: "2020-04-07T08:30:00-05:00"
    },
    registrant: {
      id: 3,
      name: "joe",
      email: "joe@toptal.com"
    },
    webinar: {
      id: 9,
      name: "Test",
      video: 5,
      published: true,
      start_date: "2020-03-01",
      stop_date: null
    }
  }
};

const eventCancelled = {
  hook: {
    id: 6,
    event: "event_cancelled",
    target: "http://localhost:5000/event_cancelled"
  },
  data: {
    id: 4,
    webinar: {
      id: 9,
      name: "Test",
      video: 5,
      published: true,
      start_date: "2020-03-01",
      stop_date: null
    },
    start_time: "2020-04-07T08:30:00-05:00",
    origin_timezone: "US/Central",
    origin_start_time: "2020-04-07T08:30:00-05:00"
  }
};

describe("triggers", () => {
  describe("new registration trigger", () => {
    it("should load registration from fake hook", done => {
      const bundle = {
        inputData: {},
        cleanedRequest: registration
      };

      appTester(App.triggers.registration.operation.perform, bundle)
        .then(results => {
          results.length.should.eql(1);

          const registration = results[0];

          registration.name.should.eql("joe");
          registration.email.should.eql("joe@toptal.com");

          registration.webinar_name.should.eql("Test");
          registration.webinar_id.should.eql(9);
          registration.event_time.should.eql("2020-04-07T08:30:00-05:00");

          done();
        })
        .catch(done);
    });

    it("should load registration from list", done => {
      const bundle = {
        meta: {
          frontend: true
        },
        authData: {
          api_key: process.env.TEST_API_KEY
        }
      };

      appTester(App.triggers.registration.operation.performList, bundle)
        .then(results => {
          results.length.should.be.greaterThan(1);

          const registration = results[0];
          registration.should.have.property("name").which.is.a.String();
          registration.should.have.property("email").which.is.a.String();
          registration.should.have.property("webinar_name").which.is.a.String();
          registration.should.have.property("webinar_id").which.is.a.Number();
          registration.should.have.property("event_time").which.is.a.String();

          done();
        })
        .catch(done);
    });
  });

  describe("registrant missed webinar trigger", () => {
    it("should load data from fake hook", done => {
      const bundle = {
        inputData: {},
        cleanedRequest: registrantMissedWebinarEvent
      };

      appTester(
        App.triggers.registrant_missed_webinar.operation.perform,
        bundle
      )
        .then(results => {
          results.length.should.eql(1);

          const registration = results[0];

          registration.name.should.eql("joe");
          registration.email.should.eql("joe@toptal.com");

          registration.webinar_name.should.eql("Test");
          registration.webinar_id.should.eql(9);
          registration.event_time.should.eql("2020-04-07T08:30:00-05:00");

          done();
        })
        .catch(done);
    });

    it("should load data from list", done => {
      const bundle = {
        meta: {
          frontend: true
        },
        authData: {
          api_key: process.env.TEST_API_KEY
        }
      };

      appTester(
        App.triggers.registrant_missed_webinar.operation.performList,
        bundle
      )
        .then(results => {
          results.length.should.be.greaterThan(1);

          const registration = results[0];
          registration.should.have.property("name").which.is.a.String();
          registration.should.have.property("email").which.is.a.String();
          registration.should.have.property("webinar_name").which.is.a.String();
          registration.should.have.property("webinar_id").which.is.a.Number();
          registration.should.have.property("event_time").which.is.a.String();

          done();
        })
        .catch(done);
    });
  });

  describe("event cancelled trigger", () => {
    it("should load data from fake hook", done => {
      const bundle = {
        inputData: {},
        cleanedRequest: eventCancelled
      };

      appTester(App.triggers.event_cancelled.operation.perform, bundle)
        .then(results => {
          results.length.should.eql(1);

          const registration = results[0];

          registration.webinar_name.should.eql("Test");
          registration.webinar_id.should.eql(9);
          registration.event_time.should.eql("2020-04-07T08:30:00-05:00");

          done();
        })
        .catch(done);
    });

    it("should load data from list", done => {
      const bundle = {
        meta: {
          frontend: true
        },
        authData: {
          api_key: process.env.TEST_API_KEY
        }
      };

      appTester(App.triggers.event_cancelled.operation.performList, bundle)
        .then(results => {
          results.length.should.be.greaterThan(1);

          const registration = results[0];
          registration.should.have.property("webinar_name").which.is.a.String();
          registration.should.have.property("webinar_id").which.is.a.Number();
          registration.should.have.property("event_time").which.is.a.String();

          done();
        })
        .catch(done);
    });
  });
});
