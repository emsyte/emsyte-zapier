# Emsyte Zapier App

Zapier app for Emsyte integration.

## Getting started

For an overview of Zapier apps, see the [docs](https://zapier.com/developer/start/introduction)

To get started, first you need to install the zapier cli:

```bash
npm install -g zapier-cli
```

Than you can install this project and run the tests:

```bash
npm install

zapier test
```

### Configuring tests to connect to backend

In order for all the tests pass, you need to create a `.env` that points to your backend.
Here's an example `.env` file assuming you have the backend running locally on port 8000:

```
API_URL=http://localhost:8000
TEST_API_KEY=<api key>
```

To get the API key, you need to hit this API endpoint on the backend `/api/v1/webhook-token/`

After creating the `.env` running `zapier test` should show that all tests pass.

## Deploying the App
To deploy the app, run `zapier push`
