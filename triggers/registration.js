const HOST = process.env.API_URL;
const ROUTE = "api/v1";
const BASE_URL = `${HOST}/${ROUTE}`;

const subscribeHook = (z, bundle) => {
  z.console.log("subscribing to registration trigger");

  const data = {
    target: bundle.targetUrl,
    event: "registration"
  };

  const options = {
    url: `${BASE_URL}/webhook/`,
    method: "POST",
    body: data
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const unsubscribeHook = (z, bundle) => {
  const hookId = bundle.subscribeData.id;
  const options = {
    url: `${BASE_URL}/webhook/${hookId}/`,
    method: "DELETE"
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const processRegistration = data => ({
  created: data.created,
  name: data.name,
  email: data.email,
  webinar_name: data.webinar.name,
  webinar_id: data.webinar.id,
  event_time: data.event.start_time
});

const getRegistration = (z, bundle) => {
  const data = bundle.cleanedRequest.data;

  return [processRegistration(data)];
};

const getFallbackRealRegistration = (z, bundle) => {
  // For the test poll, you should get some real data, to aid the setup process.
  const options = {
    url: `${BASE_URL}/feed/registrations/`
  };

  return z
    .request(options)
    .then(response =>
      JSON.parse(response.content).results.map(processRegistration)
    );
};

module.exports = {
  key: "registration",
  noun: "Registration",
  display: {
    label: "New Registration",
    description: "Triggers when someone registers for a webinar."
  },

  operation: {
    inputFields: [],

    type: "hook",

    performSubscribe: subscribeHook,
    performUnsubscribe: unsubscribeHook,

    perform: getRegistration,
    performList: getFallbackRealRegistration,

    sample: {
      created: "2020-03-26T23:13:42.173228-05:00",
      name: "Joe Smith",
      email: "joe@example.com",
      webinar_name: "Test Webinar",
      webinar_id: 1,
      event_time: "2020-04-07T08:30:00-05:00"
    },

    outputFields: [
      { key: "created", label: "Created" },
      { key: "name", label: "Name" },
      { key: "email", label: "Email" },
      { key: "webinar_name", label: "Webinar Name" },
      { key: "webinar_id", type: "integer", label: "Webinar ID" },
      { key: "event_time", type: "datetime", label: "Event Start Time" }
    ]
  }
};
