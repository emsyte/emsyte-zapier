const HOST = process.env.API_URL;
const ROUTE = "api/v1";
const BASE_URL = `${HOST}/${ROUTE}`;

const subscribeHook = (z, bundle) => {
  z.console.log("subscribing to event_cancelled trigger");

  const data = {
    target: bundle.targetUrl,
    event: "event_cancelled"
  };

  const options = {
    url: `${BASE_URL}/webhook/`,
    method: "POST",
    body: data
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const unsubscribeHook = (z, bundle) => {
  const hookId = bundle.subscribeData.id;
  const options = {
    url: `${BASE_URL}/webhook/${hookId}/`,
    method: "DELETE"
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const processCancelledEvent = data => ({
  // Modified is going to be the time the event was cancelled, so we treat it as the trigger time.
  created: data.modified,
  webinar_name: data.webinar.name,
  webinar_id: data.webinar.id,
  event_time: data.start_time
});

const getCancelledEvent = (z, bundle) => {
  const data = bundle.cleanedRequest.data;

  return [processCancelledEvent(data)];
};

const getFallbackRealCancelledEvent = (z, bundle) => {
  // For the test poll, you should get some real data, to aid the setup process.
  const options = {
    url: `${BASE_URL}/feed/cancelled-events/`
  };

  return z
    .request(options)
    .then(response =>
      JSON.parse(response.content).results.map(processCancelledEvent)
    );
};

module.exports = {
  key: "event_cancelled",
  noun: "Event Cancellation",
  display: {
    label: "Webinar Event Cancelled",
    description: "Triggers when the webinar author cancels an event."
  },

  operation: {
    inputFields: [],

    type: "hook",

    performSubscribe: subscribeHook,
    performUnsubscribe: unsubscribeHook,

    perform: getCancelledEvent,
    performList: getFallbackRealCancelledEvent,

    sample: {
      id: 4,
      modified: "2020-03-27T15:21:06.285544-05:00",
      webinar: {
        id: 9,
        name: "Test",
        video: 5,
        published: true,
        start_date: "2020-03-01",
        stop_date: null
      },
      start_time: "2020-04-07T08:30:00-05:00",
      origin_timezone: "US/Central",
      origin_start_time: "2020-04-07T08:30:00-05:00"
    },

    outputFields: [
      { key: "created", label: "Created" },
      { key: "webinar_name", label: "Webinar Name" },
      { key: "webinar_id", type: "integer", label: "Webinar ID" },
      { key: "event_time", type: "datetime", label: "Event Start Time" }
    ]
  }
};
