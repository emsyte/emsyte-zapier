const HOST = process.env.API_URL;
const ROUTE = "api/v1";
const BASE_URL = `${HOST}/${ROUTE}`;

const subscribeHook = (z, bundle) => {
  z.console.log("subscribing to registrant_missed_webinar trigger");

  const data = {
    target: bundle.targetUrl,
    event: "registrant_missed_webinar"
  };

  const options = {
    url: `${BASE_URL}/webhook/`,
    method: "POST",
    body: data
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const unsubscribeHook = (z, bundle) => {
  const hookId = bundle.subscribeData.id;
  const options = {
    url: `${BASE_URL}/webhook/${hookId}/`,
    method: "DELETE"
  };

  return z.request(options).then(response => JSON.parse(response.content));
};

const processRegistrantMissedWebinar = data => ({
  created: data.created,
  name: data.registrant.name,
  email: data.registrant.email,
  webinar_name: data.webinar.name,
  webinar_id: data.webinar.id,
  event_time: data.event.start_time
});

const getRegistrantMissedWebinar = (z, bundle) => {
  const data = bundle.cleanedRequest.data;

  return [processRegistrantMissedWebinar(data)];
};

const getFallbackRealRegistrantMissedWebinar = (z, bundle) => {
  // For the test poll, you should get some real data, to aid the setup process.
  const options = {
    url: `${BASE_URL}/feed/registrant-missed-webinar/`
  };

  return z
    .request(options)
    .then(response =>
      JSON.parse(response.content).results.map(processRegistrantMissedWebinar)
    );
};

module.exports = {
  key: "registrant_missed_webinar",
  noun: "RegistrantMissedWebinarEvent",
  display: {
    label: "Registrant Missed Webinar",
    description:
      "Triggers when someone who was registered for a webinar never views it."
  },

  operation: {
    inputFields: [],

    type: "hook",

    performSubscribe: subscribeHook,
    performUnsubscribe: unsubscribeHook,

    perform: getRegistrantMissedWebinar,
    performList: getFallbackRealRegistrantMissedWebinar,

    sample: {
      created: "2020-03-26T23:13:42.173228-05:00",
      name: "Joe Smith",
      email: "joe@example.com",
      webinar_name: "Test Webinar",
      webinar_id: 1,
      event_time: "2020-04-07T08:30:00-05:00"
    },

    outputFields: [
      { key: "created", label: "Created" },
      { key: "name", label: "Name" },
      { key: "email", label: "Email" },
      { key: "webinar_name", label: "Webinar Name" },
      { key: "webinar_id", type: "integer", label: "Webinar ID" },
      { key: "event_time", type: "datetime", label: "Event Start Time" }
    ]
  }
};
