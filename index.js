const registration = require("./triggers/registration");
const registrant_missed_webinar = require("./triggers/registrant_missed_webinar");
const event_cancelled = require("./triggers/event_cancelled");

const { authConfig, addApiKeyToHeader } = require("./authentication");

// Now we can roll up all our behaviors in an App.
const App = {
  // This is just shorthand to reference the installed dependencies you have. Zapier will
  // need to know these before we can upload
  version: require("./package.json").version,
  platformVersion: require("zapier-platform-core").version,

  authentication: authConfig,

  beforeRequest: [addApiKeyToHeader],

  afterResponse: [],

  resources: {},

  // If you want your trigger to show up, you better include it here!
  triggers: {
    [registration.key]: registration,
    [registrant_missed_webinar.key]: registrant_missed_webinar,
    [event_cancelled.key]: event_cancelled
  },

  // If you want your searches to show up, you better include it here!
  searches: {},

  // If you want your creates to show up, you better include it here!
  creates: {}
};

// Finally, export the app.
module.exports = App;
