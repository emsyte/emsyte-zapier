const HOST = process.env.API_URL;
const ROUTE = "api/v1";
const BASE_URL = `${HOST}/${ROUTE}`;

const authConfig = {
  type: "custom",
  fields: [
    { key: "api_key", label: "API Key", required: true, type: "string" }
  ],
  test: {
    method: "GET",
    url: `${BASE_URL}/webhook/`
  },
  connectionLabel: "{{bundle.authData.username}}"
};

const addApiKeyToHeader = (request, z, bundle) => {
  request.headers.Authorization = `Token ${bundle.authData.api_key}`;
  return request;
};

module.exports = {
  authConfig,
  addApiKeyToHeader
};
